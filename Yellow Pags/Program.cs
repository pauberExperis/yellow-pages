﻿using System;
using System.Collections.Generic;

namespace Yellow_Pags
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    class Program
    {
        public static Dictionary<int, Person> people = new Dictionary<int, Person>
        {
            {
                0, new Person { FirstName="Jenny", LastName="Helman"}
            },
            {
                1, new Person { FirstName="Chris", LastName="Tusse"}
            },
            {
                2, new Person { FirstName="Foo", LastName="Faa"}
            },
            {
                3, new Person { FirstName="Bar", LastName="Fight"}
            },
            {
                4, new Person { FirstName="Source", LastName="Control"}
            }
        };

        static void Main(string[] args)
        {
            StartMethod();
            
        }
        public static void StartMethod()
        {
            Console.WriteLine("Enter name: ");
            string userInput = Console.ReadLine();
            for (int i = 0; i < people.Count; i++)
            {
                string tempName = people[i].FirstName + " " + people[i].LastName;
                if (tempName.ToLower().Contains(userInput.ToLower()))
                {
                    Console.WriteLine("Match found! Name: " + tempName);
                } else
                {
                    Console.WriteLine("No matches found");
                }

            }

            EndMethod();
        }

        public static void EndMethod()
        {
            Console.WriteLine("Enter Y to try again, or X to exit");
            string result = Console.ReadLine();
            if (result.ToLower() == "y")
            {
                StartMethod();
            }
            else if (result.ToLower() == "x")
            {
                Console.WriteLine("Goodbye!");
            }
            else
            {
                Console.WriteLine("Unrecognised input, try again: ");
                EndMethod();
            }
        }
    }
}
